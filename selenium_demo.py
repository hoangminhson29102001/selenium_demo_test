import selenium
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
import time
import pandas as pd
import csv
#from webdriver_manager.chrome import ChromeDriverManager


data_frame = pd.DataFrame()
with open('new1.csv','a',encoding="utf-8") as f_object:
    writer_object = csv.writer(f_object)
    writer_object.writerow(["Category","Feedback","Rate"])

########################
#open Chrome and loading webpage
driver = webdriver.Chrome('C:\chromedriver_win32\chromedriver.exe')
#navigating to shopee wwebpage
driver.get("https://shopee.vn/")
driver.maximize_window()
#find element want to interacting
error_catching = 0
try:
    #find element that referred to close ad button
    close_ad = WebDriverWait(driver,10).until(
        EC.presence_of_element_located((By.CLASS_NAME,"shopee-popup__close-btn"))
    )
    #click button to close pop up ad
    close_ad.click()
    #navigate to category1- thoi trang nam
    categories = WebDriverWait(driver,15).until(
        EC.presence_of_all_elements_located((By.CLASS_NAME,"_13sfos"))  #find class name of categories
    )
    list_category = [] #array to store name of categories
    #Take category 
    count = 0
    for category in categories:
        a = category.text
        list_category.append(a)
    len_category = len(list_category)
    for product in list_category:
        # count +=1
        # if(count <=6):(         CÁI NÀY DO MÁY EM NÓ CÙI KHÔNG TREO ĐƯỢC NÊN PHẢI ĐẶT ĐỂ CRAWL TIẾP PHẦN
        #                               CHƯA CRAWL ĐƯỢC )
        #     continue
        # Clicking to chosing category
        time.sleep(3)
        #Put an exception handle to close popup ad
        try:
            close = driver.find_element_by_class_name("shopee-popup__close-btn")
            time.sleep(5)
            close.click()
        except:
            error_catching +=1
        #Click to navigating to categories
        link = driver.find_element_by_link_text(product)
        link.click()
        #Click to chosing product
        time.sleep(5)
        driver.execute_script("window.scrollBy(0,2000)","")
        time.sleep(4)
        driver.execute_script("window.scrollBy(0,1000)","")
        time.sleep(4)
        driver.execute_script("window.scrollBy(0,500)","")
        time.sleep(4)
        driver.execute_script("window.scrollBy(0,1000)","")
        time.sleep(4)
        new_products = WebDriverWait(driver,10).until(
            EC.presence_of_all_elements_located((By.CLASS_NAME,"_35LNwy"))      #find classname of products
        )
        number_of_products = len(new_products)
        #chose product 
        for i in range(1,number_of_products+1):
            y = str(i)      #index in xpath of product
            new_product = WebDriverWait(driver,10).until(
                EC.presence_of_element_located((By.XPATH,'//*[@id="main"]/div/div[2]/div[2]/div[4]/div[2]/div/div[2]/div['+y+']'))
            )
            new_product.click()         #click to view comment of product
            #SCroll down to load the comment of customer
            time.sleep(4)
            driver.execute_script("window.scrollBy(0,1000)","")
            time.sleep(4)
            driver.execute_script("window.scrollBy(0,1800)","")
            time.sleep(4)
            list_comments = []
            list_num_rating = []
            try:
                #Go to comment page of 5-4-3-2-1 stars rating
                for num_rate in range(2,7):
                    Overview_rating = WebDriverWait(driver,3).until(
                        EC.presence_of_element_located((By.XPATH,'//*[@id="main"]/div/div[2]/div[2]/div[2]/div[3]/div[2]/div[1]/div[2]/div/div[2]/div[2]/div['+str(num_rate)+']'))
                    )
                    print("oke hehe")
                    time.sleep(3)
                    Overview_rating.click()
                    error_catching = 0
                    #Take the feedback of customer
                    try:
                        comments = WebDriverWait(driver,3).until(
                            EC.presence_of_all_elements_located((By.CLASS_NAME,"shopee-product-rating__content"))
                        )
                        #Take number of rating stars
                        for rating in driver.find_elements_by_css_selector('.shopee-product-rating'):
                            stars = rating.find_elements_by_css_selector('.icon-rating-solid')
                            num = len(stars)
                            list_num_rating.append(num)
                        for comment in comments:
                            string = comment.text
                            list_comments.append(string)

                        number_feedback = len(list_num_rating)
                    except:
                        error_catching =1
                dict = {'Category':product,'Feedback': list_comments,'Rate': list_num_rating}
                df = pd.DataFrame(dict)
                df.to_csv('new1.csv',mode= 'a',header = False, index = False)
            except:
                error_catching =1
            # print(list_comments)
            # print(list_num_rating)
            #write csv file

            time.sleep(5)
            driver.back()
            time.sleep(5)
        print("thành cong")

        # time.sleep(3)
        driver.back()
        time.sleep(3)
        # driver.back()
    
except:
    print("An error has occur")
    driver.quit()


